/**
 *  file_reader is a pipe writer and
 *  file_writer is reading from the pipe.
 */
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>



#define BUFSZ 256




int file_reader(const int input_file_fd, const int pipe_write_fd)
{
    char buffer[BUFSZ];
    int read_n;

    while ((read_n = read(input_file_fd, buffer, BUFSZ)))
    {
        if (!(write(pipe_write_fd, buffer, read_n))) {
            perror("pipe write error");
            return EXIT_FAILURE;
        } else {
            printf("wrote %li bytes to pipe\n", read_n);
        }
    }
    return EXIT_SUCCESS;
}




int file_writer(const int output_file_fd, const int pipe_read_fd)
{
    char buffer[BUFSZ];
    int read_n;

    while ((read_n = read(pipe_read_fd, buffer, BUFSZ)))
    {
        printf("read %li bytes from pipe\n", read_n);

        if (!(write(output_file_fd, buffer, read_n))) {
            perror("file write error");
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}




int main(const int argc, char** argv)
{
    if (argc < 3) {
        printf("please pass two files as parameters\n");
        return EXIT_FAILURE;
    }

    // files
    const int input_file = open(argv[1], O_RDONLY);
    const int output_file = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC);

    // pipe
    int pdesc[2];

    if (pipe(pdesc)) {
        perror("pipe error");
    }

    const int pipe_read_fd = pdesc[0];
    const int pipe_write_fd = pdesc[1];

    // fork
    unsigned child_count = 0;

    pid_t pid = fork();

    if (pid < 0) {
        perror("fork error");
    }
    else if (pid != 0)
    {
        // we're in the parent process after creating the first child
        pid = fork();
        if (pid < 0) {
            perror("fork error");
        }
        else if (pid == 0) {
            // we're in the second child process
            close(pipe_write_fd);
            return file_writer(output_file, pipe_read_fd);
        }
    }
    else
    {
        // we're in the first child process
        close(pipe_read_fd);
        return file_reader(input_file, pipe_write_fd);
    }

    return EXIT_SUCCESS;
}
