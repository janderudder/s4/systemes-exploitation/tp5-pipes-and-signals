#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>


void child_handler(int sig)
{
    printf("Reçu le signal SIGCHLD (%d)\n", sig);
}


int main(void)
{
    pid_t pid = fork();

    if (pid < 0) {
        perror("erreur fork");
    }
    else if (pid != 0) { // parent
        signal(SIGCHLD, child_handler);
        while (1) {
            pause();
        }
    }
    else { // child
        printf("<processus fils> mon pid: %d\n", getpid());
        sleep(1);
        printf("<processus fils> se termine\n");
    }
    return EXIT_SUCCESS;
}
