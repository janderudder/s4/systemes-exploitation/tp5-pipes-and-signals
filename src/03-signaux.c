/*
    SIGRTMIN to SIGRTMAX are signal numbers reserved to custom use
    by applications.
*/
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <signal.h>


/*
    POSIX compliant way of getting max signal is macro SIGRTMAX.
    But it's a macro function, so cannot use it as array size.
*/
size_t signalCounters[_NSIG] = {0};



void catchSignal(int sig)
{
    if (sig < SIGRTMAX) {
        signalCounters[sig] += 1;
        printf("reception d'un signal: %d\n", sig);
    }
}



int main(void)
{
    size_t installedHandlers = 0;

    for (int isig=1; isig < SIGRTMAX; ++isig)
    {
        if (signal(isig, catchSignal) == SIG_ERR) {
            printf("Je ne peux pas capturer le signal n°%d\n", isig);
        } else {
            installedHandlers += 1;
        }
    }

    printf("Compteur de signal installé pour %lu / %d signaux.\n", installedHandlers, SIGRTMAX);
    printf("Prêt à recevoir des signaux.\n");

    while (1) {     // this is where we catch signals
        pause();
    }

    for (int isig=1; isig < SIGRTMAX; ++isig) {
        if (signalCounters[isig] != 0) {
            printf("signal %d reçu %lu fois\n", isig, signalCounters[isig]);
        }
    }

    return EXIT_SUCCESS;
}
