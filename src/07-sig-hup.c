#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

// strangely, I have to define this to get the kill function (also gives sigaction).
#define __USE_POSIX

#include <signal.h>
#include <sys/wait.h>
#include <fcntl.h>


void hup_handler(int sig)
{
    FILE* file;
    file = fopen("sighup.txt", "w");
    fprintf(file, "Signal SIG_HUP reçu.\n");
    fclose(file);
    exit(EXIT_SUCCESS);
}



int main(void)
{
    if (signal(SIGHUP, hup_handler) == SIG_ERR) {
        printf("erreur à l'installation du gestionnaire de SIG_HUP.\n");
        return EXIT_FAILURE;
    }
    else {
        printf("gestionnaire de SIG_HUP installé\n");
    }

    pause();

    return EXIT_SUCCESS;
}
